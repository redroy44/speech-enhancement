#!/usr/bin/env python

"""Module summary

This is the module that automatically processes julius .out files to
evaluate Word Error Rate score across different backgrounds

"""
from optparse import OptionParser
import os
import sys
import multiprocessing as mp
import time
from fnmatch import fnmatch
from levenshtein import levenshtein
import logging

def main(cmdline=None):
    """The main function for multithread file processing.
    
    """
    parser = make_parser()
    opts, args = parser.parse_args(cmdline)

    inputdir = str(opts.inputdir)
    outputdir = str(opts.outputdir)

    print inputdir
    print outputdir

    fileList = listFiles(opts.inputdir, opts.snrLevel)

    print len(fileList)

    # create an array of ThreadPools of length fileList
    #log_to_stderr(logging.DEBUG)
    lock = mp.Lock()
    queue = mp.Queue()
    jobs = []
    for i in range(len(fileList)):
        p = mp.Process(target=worker, args=(fileList[i], opts.inputdir, opts.outputdir, lock, queue))
        jobs.append(p)
        p.start()

    for i in range(len(jobs)):
        print "From queue: ", queue.get()
          
    queue.close()
    queue.join_thread()

    for p in jobs:
        p.join()

    #print "\nTotal number of errors: %d, total number of words: %d" % (sum_errors, sum_words)
    #print "Word error rate: %.2f %%" % (float(sum_errors) / sum_words * 100.0)
    return 0

def worker(fList, inputdir, outputdir, lock, queue):

    sum_words = 0
    sum_errors = 0
    fList = list(fList)
    for input in fList:

        output = str(input).replace("txt","out").replace(inputdir, outputdir)

        txtfile = open(input, "r")
        txt = txtfile.readline().split()
        txtfile.close()
        # open julius .out file
        outfile = open(output, "r")
        outline = outfile.readline()
        outfile.close()

        out = outline.split(" ")[2:-1]
        # count words
        words = len(out)
        # perform levenshtein(source, target)
        dist = levenshtein(txt, out)

        # return LV distance and reference word count
        sum_words += words
        sum_errors += dist

        queue.put([sum_errors, sum_words])

def pool_worker(inputfile):

    outputfile = inputfile.replace("txt","out")
    outputfile = outputfile.replace(inputdir, outputdir)
    print outputfile
    # now open reference .txt file
    txtfile = open(inputfile, "r")
    txt = txtfile.readline().split()
    txtfile.close()
    # open julius .out file
    outfile = open(outputfile, "r")
    outline = outfile.readline()
    outfile.close()

    out = outline.split(" ")[2:-1]
    # count words
    words = len(out)
    # perform levenshtein(source, target)
    dist = levenshtein(txt, out)

    # TODO prinr current values of words and dist

    # return LV distance and reference word count
    return dist, words
 

def make_parser():
    """Construct an option parser"""

    usage = """batchProcessing.py args
            -i, --inputdir     - specify input directory
            -o, --outputdir    - specify output directory
            -n, --numThreads   - specify number of threads
            -s, --snrLevel     - specify SNR level [0dB, 5dB, 10dB, 15dB, clean]
Sometimes you might explain the purpose of this program as well.
"""
    
    parser = OptionParser(usage)

    parser.add_option("-i", "--inputdir", dest="inputdir", help="input directory")
    parser.add_option("-o", "--outputdir", dest="outputdir", help="output directory")
    parser.add_option("-n", "--numThreads", dest="numThreads", help="specify number of threads")
    parser.add_option("-s", "--snrLevel", dest="snrLevel", type='choice', choices=['0dB', '5dB', '10dB', '15dB', 'clean'],
                      help="specify SNR level [0dB, 5dB, 10dB, 15dB, clean]") 

    parser.set_defaults(inputdir="C:\\Users\\pbandurs\\Personal Data\\Documents\\audio_files", 
                        outputdir="C:\\Users\\pbandurs\\Personal Data\\Documents\\audio_files\\output",
                        numThreads=4,
                        snrLevel="0dB")
    return parser

def listFiles(indir, snr):
    
    pattern = "*.txt"
    root = os.path.join(indir, snr)

    subdirList =  os.listdir(root)
    fileList = [[],[]]#[] * len(subdirList)
    for i in range(len(subdirList)):
        subroot = os.path.join(root, subdirList[i])
        for path, subdirs, files in os.walk(subroot):
            for name in files:
                if fnmatch(name, pattern):
                # TODO? check if output file already exists
                    fileList[i].append(os.path.join(path, name))
    return fileList

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))