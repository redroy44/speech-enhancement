#!/usr/bin/env python

"""Module summary

This is the module that automatically processes wavefiles from the specified directory.

First it performs the speech enhancement.
Next, the enhanced file is sent to the Julius ASR.
Finally, the levenshtein distance is measured to assess the WER measure.
"""
from optparse import OptionParser
import os
import sys
import subprocess
from multiprocessing import current_process, Process
from multiprocessing import Pool as ThreadPool
#from multiprocessing.pool import ThreadPool
import time
from fnmatch import fnmatch
from shutil import copyfile
from levenshtein import levenshtein


# julius paths
juliusPath = "c:\\julius"
juliusExe = os.path.join(juliusPath, "julius.exe")
juliusConfig = os.path.join(juliusPath, "julius.jconf")

def main(cmdline=None):
    """The main function for multithread batch processing.
    
    """
    parser = make_parser()
    opts, args = parser.parse_args(cmdline)

    print "Input directory: ", opts.inputdir
    print "Output directory: ", opts.outputdir

    makeOutdirs(opts.inputdir, opts.outputdir, opts.snrLevel)
    fileList = listFiles(opts.inputdir, opts.snrLevel)

    # thread args   
    args = ([i, i.replace(opts.inputdir, opts.outputdir), opts.enhance] for i in fileList)

    # time threads
    t0 = time.time()
    pool=ThreadPool(processes=opts.numThreads)
    pool_outputs = pool.map(worker, args)
    pool.close()
    pool.join()
    t1 = time.time()
    total = t1 - t0
    print "\nPoolthreads finished in time: %.3f seconds" % (total)

    # sum errors and words in every thread
    sum_words = 0
    sum_errors = 0

    for e, w in pool_outputs:
        sum_words += w
        sum_errors += e

    print "\nTotal number of errors: %d, total number of words: %d" % (sum_errors, sum_words)
    print "Word error rate: %.2f %%" % (float(sum_errors) / sum_words * 100.0)
    return 0

def worker(args):
    # unpack args
    inputfile, outputfile, enhance = args
    print current_process().name, "is processing file: ", inputfile
    #print "arg2: ", outputfile
    #print "arg3: ", enhance

    # now, call enhancement subprocess if enhance flag is set else copy input file to out directory
    if enhance:
        outputfile = outputfile.replace(".wav", "_enhanced.wav")

        proc = subprocess.Popen(['../x64/Debug/Enhancement.exe', '--inputfile', inputfile, '--outputfile', outputfile], 
                            stdout=subprocess.PIPE)
        proc.wait()
    else:
        copyfile(inputfile, outputfile)

    juliusproc = subprocess.Popen([juliusExe, '-C', juliusConfig, '-input', 'rawfile'],
                                  stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)

    juliusoutput = juliusproc.communicate(outputfile)
    juliusproc.wait()

    # open a file
    log = open(outputfile.replace(".wav", ".log"), "w")
    # print julius output to log file
    for line in juliusoutput:
        log.write(str(line))
    log.close()

    # now open reference .txt file
    txtfile = open(inputfile.replace(".wav", ".txt"), "r")
    txt = txtfile.readline().split()
    txtfile.close()
    # open julius .out file
    outfile = open(outputfile.replace(".wav", ".out"), "r")
    outline = outfile.readline()
    outfile.close()

    out = outline.split(" ")[2:-1]
    # count words
    words = len(txt)
    # perform levenshtein(source, target)
    dist = levenshtein(txt, out)

    # return LV distance and reference word count
    return dist, words

def make_parser():
    """Construct an option parser"""

    usage = """batchProcessing.py args
            -i, --inputdir     - specify input directory
            -o, --outputdir    - specify output directory
            -e, --enhance      - turn on speech enhanement
            -n, --numThreads   - specify number of threads
            -s, --snrLevel     - specify SNR level [0dB, 5dB, 10dB, 15dB, clean]
Sometimes you might explain the purpose of this program as well.
"""
    
    parser = OptionParser(usage)

    parser.add_option("-i", "--inputdir", dest="inputdir", help="input directory")
    parser.add_option("-o", "--outputdir", dest="outputdir", help="output directory")
    parser.add_option("-e", "--enhance", help="turn on speech enhancement",
                      action="store_true", dest="enhance")
    parser.add_option("-n", "--numThreads", dest="numThreads", help="specify number of threads")
    parser.add_option("-s", "--snrLevel", dest="snrLevel", type='choice', choices=['0dB', '5dB', '10dB', '15dB', 'clean'],
                      help="specify SNR level [0dB, 5dB, 10dB, 15dB, clean]") 

    parser.set_defaults(inputdir="C:\\Users\\pbandurs\\Personal Data\\Documents\\audio_files", 
                        outputdir="C:\\Users\\pbandurs\\Personal Data\\Documents\\audio_files\\output",
                        enhance=False,
                        numThreads=4,
                        snrLevel="0dB")
    return parser

def makeOutdirs(inputdir, outdir, snr):

    if not os.path.isdir(outdir):
        os.mkdir(outdir)
        print "outputdir created"

    outroot = os.path.join(outdir, snr)
    if not os.path.isdir(outroot):
        os.mkdir(outroot)
        print "root created"
    
    inroot = os.path.join(inputdir, snr)
    for path, subdirs, files in os.walk(inroot):
        for subdir in subdirs:
            if not os.path.isdir(os.path.join(path.replace(inputdir, outdir), subdir)):
                os.mkdir(os.path.join(path.replace(inputdir, outdir), subdir))
                print "subdir created"

def listFiles(indir, snr):
    
    fileList = []
    pattern = "*.wav"
    root = os.path.join(indir, snr)
    for path, subdirs, files in os.walk(root):
        for name in files:
            if fnmatch(name, pattern):
                # TODO? check if output file already exists
                fileList.append(os.path.join(path, name))
    return fileList

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))