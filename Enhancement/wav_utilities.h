#ifndef WAV_UTILITIES_H
#define WAV_UTILITIES_H
#include <armadillo>

arma::mat segment_wav(arma::vec, int, float);
arma::vec overlap_add(arma::mat, arma::mat, int, float); // segments, phase, win_len, overlap

arma::vec hamming_window(int);
arma::vec hanning_window(int);

arma::mat window_filter(arma::mat, arma::vec); // segments, window

arma::mat get_phase(arma::cx_mat); // atan2(x.imag(), x.real());
arma::cx_mat get_complex(arma::mat, arma::mat); // polar to cartesian
#endif