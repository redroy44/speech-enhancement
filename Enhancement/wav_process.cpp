#include <wav_process.h>

using namespace arma;

const int BUFFER_LEN = 1024;

vec read_wav(const char * infilename)
{

    SndfileHandle infile = SndfileHandle(infilename);
    if (!infile) {
        std::cout << "Cannot load the file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    
    std::cout << infilename << std::endl;
    std::cout << infile.channels() << std::endl;
    std::cout << infile.samplerate() << std::endl;
    std::cout << infile.frames() << std::endl;
    std::cout << infile.format() << std::endl;

    static float buffer[BUFFER_LEN];
    vec wave = colvec((const arma::uword)infile.frames());
    
    for (int i = 0; i < infile.frames(); i++) {
        if ((i % BUFFER_LEN) == 0) {
            infile.read(buffer, BUFFER_LEN);
        }

        wave(i) = buffer[i % BUFFER_LEN];
        // std::cout << i << " " << wave(i) << std::endl;
    }
    
    // TODO: return struct (wave_options)!

    return wave;
}
void write_wav(const char *outfilename, vec wave)
{
    const int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    //const int format = 65538;// SF_FORMAT_WAV | SF_FORMAT_FLOAT;
    const int channels = 1;
    const int sampleRate = 16000;
    
    
    SndfileHandle outfile(outfilename, SFM_WRITE, format, channels, sampleRate);
    if (!outfile) {
        std::cout << "Cannot write to file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    outfile.write(wave.colptr(0), wave.n_elem);
}