#ifndef WAV_PROCESS_H
#define WAV_PROCESS_H
#include <sndfile.hh>
#include <armadillo>

arma::vec read_wav(const char *);
void write_wav(const char *, arma::vec);
#endif