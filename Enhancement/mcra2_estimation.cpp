#include <mcra2_estimation.h>


using namespace arma;

mcra2_params initialize_mcra2(vec spectrum){
    mcra2_params p;

    int length = spectrum.n_rows;
    p.eta = 0.7; // smoothing constant
    p.gamma = 0.998; // constant
    p.beta = 0.8; // constant
    p.alpha_p = 0.2; // smoothing constant
    p.alpha_d  = 0.85; // constant
    p.delta = 2 * ones<vec>(length); // frequency-dependent speech-presence threshold
    p.alpha_s = zeros<vec>(length); // (7) time-frequency dependent smoothing factor

    p.prevSmPower = spectrum;
    p.smPower = spectrum; // (2) smoothed noisy speech power spectrum
    p.prevMinPower = spectrum;
    p.minPower = spectrum; // (3) local minimum of noisy speech power spectrum
    p.noisyRatio = zeros<vec>(length); // (4) power spectrum to local minimum ratio
    p.spDecision = zeros<vec>(length); // (5) speech-presence decision
    p.prevSpProbability = zeros<vec>(length);
    p.spProbability = zeros<vec>(length); // (6) speech-presence probability
    p.prevNoiseSpectrum = spectrum;
    p.noiseSpectrum = spectrum; // (8) noise spectrum estimate


    // remove hardcoded samplerate
    int bin1k = (1 * length) / 16; // 16k samplerate 1kbin
    int  bin3k = (3 * length) / 16; // 16k samplerate 3k bin
    cout << "bin1k: " << bin1k << " bin3k: " << bin3k << endl;
    //initialize delta eq(10.5)
    for (unsigned int i = 48; i < spectrum.n_rows; i++) {
        p.delta(i) = 5;
    }

    return p;
}



mcra2_params noise_estimator(mcra2_params p, vec spectrum)
{    
    p.smPower = (p.eta * p.prevSmPower) + ((1 - p.eta) * spectrum); // eq (2)
    
    for (unsigned int i = 0; i < p.minPower.n_rows; i++) { // begin eq (3)
        if (p.prevMinPower(i) < p.smPower(i)) {
            p.minPower(i) = (p.gamma * p.prevMinPower(i)) +
               (((1 - p.gamma) / (1 - p.beta)) * (p.smPower(i) - p.beta * p.prevSmPower(i)));
        }
        else {
            p.minPower(i) = p.smPower(i);
        }
    } // end eq (3)

    p.noisyRatio = p.smPower / p.minPower; // eq (4)

    for (unsigned int i = 0; i < p.spDecision.n_rows; i++) { // begin eq (5)
        if (p.noisyRatio(i) > p.delta(i)) {
            p.spDecision(i) = 1;
        }
        else {
            p.spDecision(i) = 0;
        }
    } // end eq (5)

    p.spProbability = p.alpha_p * p.prevSpProbability + (1 - p.alpha_p) * p.spDecision; // eq (6)

    p.alpha_s = repmat(p.alpha_d, p.alpha_s.n_rows, 1) + (1 - repmat(p.alpha_d, p.alpha_s.n_rows, 1)) % p.spProbability; // eq (7) alpha_d <= alpha_s <= 1

    p.noiseSpectrum = p.alpha_s % p.prevNoiseSpectrum + (1 - p.alpha_s) % spectrum; // eq (8)

    p.prevSmPower = p.smPower; // update mcra2 parameters
    p.prevMinPower = p.minPower;
    p.prevSpProbability = p.spProbability; // redundant
    p.prevNoiseSpectrum = p.noiseSpectrum; // redundant
 
    return p;
}