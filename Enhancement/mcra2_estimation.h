#ifndef MCRA2_ESTIMATION_H
#define MCRA2_ESTIMATION_H
#include <armadillo>


struct mcra2_params{    // mcra2 parameters structure
    double eta; // smoothing constant
    double gamma; // constant
    double beta; // constant
    double alpha_p; // smoothing constant
    arma::mat alpha_d; // constant
    arma::vec delta; // frequency-dependent speech-presence threshold
    arma::vec alpha_s; // (7) time-frequency dependent smoothing factor

    arma::vec prevSmPower;
    arma::vec smPower; // (2) smoothed noisy speech power spectrum
    arma::vec prevMinPower;
    arma::vec minPower; // (3) local minimum of noisy speech power spectrum
    // arma::vec spectrum; // noisy speech power spectrum
    arma::vec noisyRatio; // (4) power spectrum to local minimum ratio
    arma::vec spDecision; // (5) speech-presence decision
    arma::vec prevSpProbability;
    arma::vec spProbability; // (6) speech-presence probability
    arma::vec prevNoiseSpectrum;
    arma::vec noiseSpectrum; // (8) noise spectrum estimate
};

mcra2_params initialize_mcra2(arma::vec); //initialize mcra2 parameters, given noisy speech power spectrum
mcra2_params noise_estimator(mcra2_params, arma::vec); // give power spectrum explicitly
#endif