#include <wav_utilities.h>
#include <armadillo>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace arma;

arma::vec hamming_window(int length)
{
    vec window = ones(length);

    for (unsigned int i = 0; i < window.n_rows; i++) {
        window(i) = 0.54 - 0.46 * cos((2*M_PI*i) / (length - 1));
    }

    return window;
}

arma::vec hanning_window(int length)
{
    vec window = ones(length);

    for (unsigned int i = 0; i < window.n_rows; i++) {
        window(i) = 0.5 * (1 - cos((2 * M_PI*i) / (length - 1)));
    }

    return window;
}

mat window_filter(mat segments, vec window)
{
   // check segment - window lengths 
    
    for (unsigned int i = 0; i < segments.n_cols; i++) {
        segments.col(i) = segments.col(i) % window; // element-wise multiplication
    }
    
    
    return segments;
}

arma::mat segment_wav(arma::vec wave, int length, float overlap)
{
   // compute number of segments based on seg length and num of audio samples
    int seg_start = static_cast<int>( length * (1 - overlap)); // (1 - overlap) is the segment shift
    int num_segments = ((wave.n_elem - length) / seg_start) + 1;
    
    mat segments = zeros(length, num_segments);
    
    // begin segmentation
    for (int i = 0; i < length; i++) {
        for (int j = 0; j < num_segments;j++) {
            segments(i, j) = wave(j*seg_start + i); 
        }
    }

    return segments;
}

vec overlap_add(mat magnitude, mat phase, int length, float overlap)
{
    
 //   magnitude.save("magnitude", raw_ascii);
    
    // reflect the first half of spectrum to get full spectrum
    if (length % 2) { // if number of spectrum bins is odd
        magnitude = join_vert(magnitude, flipud(magnitude.rows(0, magnitude.n_rows )));
    }
    else {
        magnitude = join_vert(magnitude, flipud(magnitude.rows(0, magnitude.n_rows-1)));
    }
    
    std::cout << "magnitude: "<< magnitude.n_rows << " " << magnitude.n_cols << std::endl;
    std::cout << "phase : " << phase.n_rows << " " << phase.n_cols << std::endl;

  //  magnitude.save("magnitude", raw_ascii);
  //  phase.save("phase", raw_ascii);

    cx_mat spectrum = get_complex(magnitude, phase); // retrieve complex spec from polar coordinates

    std::cout << "complex spectrum : " << spectrum.n_rows << " " << spectrum.n_cols << std::endl;

    // do the overlap-add reconstruction

    int seg_shift = static_cast<int>(length * (1 - overlap)); // (1 - overlap) is the segment shift
    vec output = zeros((spectrum.n_cols - 1)*seg_shift + length);
     
    std::cout << "overlap-add: " << output.n_elem << std::endl;

    for (unsigned int i = 0; i < spectrum.n_cols; i++) {
        int start = i*seg_shift;
        cx_vec spec = spectrum.col(i);
        output.rows(start, start + length - 1) = output.rows(start, start + length - 1) + real(ifft(spec, length));
    }

    return output;
}

mat get_phase(cx_mat spectrum)
{
    mat angle;
    angle.copy_size(spectrum);

    for (unsigned int i = 0; i < spectrum.n_rows; i++) {
        for (unsigned int j = 0; j < spectrum.n_cols; j++) {
            angle(i, j) = atan2(spectrum(i, j).imag(), spectrum(i, j).real());
        }
    }

    return angle;
}

arma::cx_mat get_complex(arma::mat magnitude, arma::mat phase)
{
    cx_mat spectrum;
    spectrum.copy_size(magnitude);

    for (unsigned int i = 0; i < spectrum.n_rows; i++) {
        for (unsigned int j = 0; j < spectrum.n_cols; j++) {
            spectrum(i, j) = std::polar(magnitude(i,j), phase(i,j));
        }
    }
   
    return spectrum;
}