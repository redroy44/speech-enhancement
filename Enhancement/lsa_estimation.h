#ifndef LSA_ESTIMATION_H
#define LSA_ESTIMATION_H
#include <boost/math/special_functions/expint.hpp>
#include <armadillo>


struct lsa_params{
    arma::vec spectrum;
    arma::vec spectrum_clean;
    arma::vec noise;
    arma::vec gain;
    arma::vec prevSNRposteriori;

    //arma::vec gainFloor;
    
    double alpha;
};
lsa_params initialize_lsa(arma::vec); // noisy spectrum estimate
arma::vec snr_decision_directed(lsa_params, arma::vec);
lsa_params lsa_estimator(lsa_params, arma::vec, arma::vec); // noisy power and noise power explicitly

arma::vec expint(arma::vec); // exponential integral of every element in vec
#endif
