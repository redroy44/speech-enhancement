#include <lsa_estimation.h>

using namespace arma;

lsa_params initialize_lsa(vec powerSpec)
{
    lsa_params p;

    p.spectrum_clean = zeros<vec>(powerSpec.n_rows);
    p.gain = ones<vec>(powerSpec.n_rows);
    p.prevSNRposteriori = ones<vec>(powerSpec.n_rows);

    //p.gainFloor = 0.001 * ones<vec>(powerSpec.n_rows);
    p.alpha = 0.97;

    return p;
}

lsa_params lsa_estimator(lsa_params parameters, vec powerSpec, vec noisePower)
{    
    parameters.spectrum = sqrt(powerSpec);
    parameters.noise = sqrt(noisePower);
    
    vec SNRposteriori = powerSpec / noisePower; // A posteriori SNR
    vec SNRpriori = snr_decision_directed(parameters, SNRposteriori); // decision-directed A priori SNR

    parameters.prevSNRposteriori = SNRposteriori; // save A posteriori SNR

    vec vk = parameters.prevSNRposteriori % (SNRpriori / (1 + SNRpriori)); // equation (8) in MMSE
    
    parameters.gain = (SNRpriori / (1 + SNRpriori)) % exp(0.5 * expint(vk)); // log-MMSE gain - equation (20) in log-MMSE ones<vec>(powerSpec.n_rows);
    
    //for (unsigned int i = 0; i < parameters.gain.n_rows; i++) {
    //    if (parameters.gain(i) < parameters.gainFloor(i)){
    //        parameters.gain(i) = parameters.gainFloor(i);
    //    }
    //}
   
    parameters.spectrum_clean = parameters.gain % parameters.spectrum; // lsa-estimate 

    return parameters;
}

vec snr_decision_directed(lsa_params parameters, vec SNRposteriori)
{
    vec SNRpriori = parameters.alpha * square(parameters.gain) % parameters.prevSNRposteriori
        + (1 - parameters.alpha) * max(SNRposteriori - 1, zeros<vec>(SNRposteriori.n_elem));
    return SNRpriori;
}

vec expint(vec vector)
{
    //static unsigned int n = 1;
    for (unsigned int i = 0; i < vector.n_rows; i++) {
        vector(i) = boost::math::expint(2, vector(i)); // vec(i) cannot be 0
    }
    return vector;
}