#include <iostream>
#include <armadillo>
#include <wav_process.h>
#include <wav_utilities.h>
#include <lsa_estimation.h>
#include <mcra2_estimation.h>
#include "boost/program_options.hpp"

// add for plotting capabilities
#include <gnuplot_i.hpp>

using namespace std;
using namespace arma;

//#define DEBUG

//void parse_args(int argc, char *argv[]) // TODO


int main(int argc, char *argv[])
{
#ifdef DEBUG
    Gnuplot g2("output wavefile");
    Gnuplot g1("input wavefile");
#endif
    namespace po = boost::program_options;
	po::options_description desc("Options");
	desc.add_options()
		("help", "Print help messages")
		("inputfile", po::value< string >(), "Input file path")
		("outputfile", po::value< string >(), "Output file path");
	
	po::variables_map vm;
	try {
		po::store(po::parse_command_line(argc, argv, desc),
			vm);

		if (vm.count("help"))
		{
			std::cout << "Basic Command Line Parameter App" << std::endl
				<< desc << std::endl;
			return 0;
		}

		po::notify(vm);
	}
	catch (po::error& e) {
		std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
		std::cerr << desc << std::endl;
		return 1;
	}

	string infile = vm["inputfile"].as<string>();
	string outfile = vm["outputfile"].as<string>();

	cout << infile << endl;
	cout << outfile << endl;

    wall_clock timer;
    timer.tic();

	vec i = read_wav(infile.c_str());


#ifdef DEBUG
    g1.set_terminal_std("windows");
    g1.set_grid();
    g1.set_xautoscale();
    g1.set_style("lines").plot_x(i, "inwavefile");
#endif
    // got the wave. can start further processing.
    // 16000 - 1s
    // x - 0.025
    int samplerate = 16000;
    float winLength = 0.025f; // 25ms
    int seg_length = (int)(samplerate * winLength); // 400 for 25ms window // some problem here !!!
    float overlap = 0.70f;// 0.066f;  // 0.7 is 70% overlap - 30% shift

    cout << "seg_length: " << seg_length << endl;

    vec hamm = hamming_window(seg_length);

    mat out = segment_wav(i, seg_length, overlap); // signal segmentation

    out = window_filter(out, hamm); // windowing
    cx_mat spectrum = fft(out); // FFT of every segment

    cout << "full_spec_length: " << spectrum.n_rows << endl;

    mat spec = abs(spectrum.rows(0,(int)(spectrum.n_rows/2)-1)); // take first half of spectrum

    cout << "half_spec_length: " << spec.n_rows << endl;
    mat phase = get_phase(spectrum); // atan2(x.imag(), x.real());

    
    // estimate noise - build the parameters structure
    // decision directed log-MMSE
    mcra2_params parameters;
    lsa_params parameters_lsa;
    mat clean_spec;
    clean_spec.copy_size(spec);

#ifdef DEBUG_PRINT
    mat noise_spectrum;
    mat noisy_spectrum;

#endif

    for (unsigned int i = 0; i < spec.n_cols; i++) {
        vec powerSpec = square(spec.col(i));
        if (i == 0) {
            parameters = initialize_mcra2(powerSpec);
            parameters_lsa = initialize_lsa(powerSpec);
        }
        else {
            parameters = noise_estimator(parameters, powerSpec);
        }
        parameters_lsa = lsa_estimator(parameters_lsa, powerSpec, parameters.noiseSpectrum); // POWER spectra
        clean_spec.col(i) = parameters_lsa.spectrum_clean;//parameters_lsa.spectrum_clean;
        
#ifdef DEBUG_PRINT   
        noisy_spectrum = join_horiz(noisy_spectrum, spec.col(i));
        noise_spectrum = join_horiz(noise_spectrum, sqrt(parameters.noiseSpectrum));

#endif
    }

    // THE LAST PART //
    vec out1 = overlap_add(clean_spec, phase, seg_length, overlap);

#ifdef DEBUG_PRINT
    cout << "Dumping data...\n";
    cout << "Estimated noise...\n";
    noise_spectrum.save("noise_spec.dat", raw_ascii);
    vec noise1 = overlap_add(noise_spectrum, phase, seg_length, overlap);
    write_wav("estimated_noise.wav", noise1);

    cout << "noisy spectrum...\n";
    noisy_spectrum.save("noisy_spec.dat", raw_ascii);
    vec noisy1 = overlap_add(noisy_spectrum, phase, seg_length, overlap);
    write_wav("noisy_no_change.wav", noisy1);

    cout << "enhanced spectrum...\n";
    clean_spec.save("enhanced_spec.dat", raw_ascii);

    cout << "clean file spectrum...\n";
    vec clean = read_wav("C:\\Users\\pbandurs\\Personal Data\\Documents\\Visual Studio 2013\\Projects\\Enhancement\\MATLAB\\dial_1124389.wav");

    mat clean_out = segment_wav(clean, seg_length, overlap); // signal segmentation

    clean_out = window_filter(clean_out, hamm); // windowing
    cx_mat cl_spectrum = fft(clean_out); // FFT of every segment
    mat cl_spec = abs(cl_spectrum.rows(0, (int)(cl_spectrum.n_rows / 2) - 1)); // take first half of spectrum
    cl_spec.save("clean_spec.dat", raw_ascii);
    cout << "Dumping data done.\n";
#endif


#ifdef DEBUG
	g2.set_terminal_std("windows");
	g2.set_grid();
	g2.set_xautoscale();
	g2.set_style("lines").plot_x(out1, "outwavefile");

	system("pause");
#endif
	write_wav(outfile.c_str(), out1);

    double n_secs = timer.toc();
    cout << "took " << n_secs << " seconds" << endl;

    return 0;
}