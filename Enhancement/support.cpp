#include <support.h>

std::vector<double> convert(arma::rowvec armaVec, std::vector<double> stdVec)
{
    for (unsigned int i = 0; i < armaVec.n_elem; i++) {
        stdVec.push_back(armaVec(i));
    }
    return stdVec;
}