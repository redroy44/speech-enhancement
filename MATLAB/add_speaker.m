% script creating new speaker in the corpus
%   inputs
%   corpus directory
%   
%
%
%
%
%

matlabpool(2) % should be 4 threads not 2

% input directory
indir = 'C:\Users\Piotrek\Copy\PRACA MASTERSKA\audio_files'
% background directory
bckdir = 'C:\Users\Piotrek\Copy\PRACA MASTERSKA\audio_files\background'
% array of snr levels
snr = [0 5 10 15];
% dictionary
data = ['ZERO ';'ONE  ';'TWO  ';'THREE';'FOUR ';...
        'FIVE ';'SIX  ';'SEVEN';'EIGHT';'NINE '];
    
dict = cellstr(data);

FS = 16000;
NBITS = 16;

% list background files
bckList = ls(bckdir)
% strip . and ..
%bckList = bckList(2:end)
%size(bckList)
bckList = bckList(3:end,:)

% speaker name
speaker_name = 'spk_test1';

%speaker_name = input('Enter speaker name: ','s');

display('Generating prompts...')

% generate prompts 5 times 10 digits from 0 to 9
prompts = randi([0 9],2,10);

% record wave, save in clean folder
recObj = audiorecorder(FS, NBITS, 1);

for i = 1 : size(prompts,1),
    disp('Start speaking: ')
    prompts(i,:)
    
    record(recObj, 20); % 20s timeout

    reply = input('Press enter to stop recording','s');
    if reply == ''
        stop(recObj);
        disp('Recording finished.')
    end
    % Store data in double-precision array.
    myRecording = getaudiodata(recObj);

    % Plot the waveform.
    %plot(myRecording);

    %save to 'clean' file
    wavwrite(myRecording, FS, NBITS, strcat(indir, '\clean\', speaker_name, '_', num2str(i), '.wav'))
    % save label
    f = fopen(strcat(indir, '\clean\', speaker_name, '_', num2str(i), '.txt'), 'w');
    outstr = '';
    for j = 1 : size(prompts,2)
        outstr = strcat(outstr, dict{prompts(i,j)+1}, {' '});
    end
    fwrite(f, outstr{1});
    fclose(f);
    % perform some normalization?
    % parallel loop, using addnoise_asl
    cleanfile = strcat(indir, '\clean\', speaker_name, '_', num2str(i), '.wav');
    source = strcat(indir, '\clean\', speaker_name, '_', num2str(i), '.txt');

    parfor a = 1: size(bckList, 1),
        temp = strtrim(bckList(a,:))
        addnoise_asl(cleanfile, strcat(indir, '\background\', bckList(a,:)), ...
            strcat(indir,  '\0dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.wav'), 0);
        addnoise_asl(cleanfile, strcat(indir, '\background\', bckList(a,:)), ...
            strcat(indir,  '\5dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.wav'), 5);
        addnoise_asl(cleanfile, strcat(indir, '\background\', bckList(a,:)), ...
            strcat(indir,  '\10dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.wav'), 10);
        addnoise_asl(cleanfile, strcat(indir, '\background\', bckList(a,:)), ...
            strcat(indir,  '\15dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.wav'), 15);
        copyfile(source, strcat(indir,  '\0dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.txt'));
        copyfile(source, strcat(indir,  '\5dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.txt'));
        copyfile(source, strcat(indir,  '\10dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.txt'));
        copyfile(source, strcat(indir,  '\15dB\', temp(1,1:end-4), '\', speaker_name, '_', num2str(i), '.txt'));
    end
    disp('Press a key.')
    pause
end

disp('Speaker added. Thank You.')
matlabpool close