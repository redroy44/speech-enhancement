#!/usr/bin/env python

from multiprocessing import Pool as ThreadPool
import time

def worker(args):

    arg1, arg2, arg3 = args # unpack args
    print " arg1: ", arg1, ", arg2: ", arg2, ", arg3: ",arg3

if __name__ == '__main__':
    
    args = ([i, 1, 2] for i in range(5))

    t0 = time.time()
    pool=ThreadPool(processes=4)
    pool.map(worker, args)
    pool.close()
    pool.join()
    t1 = time.time()
    total = t1 - t0
    
    print "poolthreads finished in time: ", total