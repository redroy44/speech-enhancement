#!/usr/bin/env python

"""Module summary

This is the module that automatically processes wavefiles from the specified directory.

First it performs the speech enhancement.
Next, the enhanced file is sent to the Julius ASR.
Finally, the levenshtein distance is measured to assess the WER measure.
"""

from optparse import OptionParser
import os
import sys
import subprocess
import levenshtein

from levenshtein import levenshtein

juliusPath = "c:\\julius\\"

juliusExe = juliusPath + "julius.exe"
juliusConfig = juliusPath + "julius.jconf"

juliusTest = juliusPath + "dial_1124389.wav"

def main(cmdline=None):
    """The main function.
    
    """
    parser = make_parser()
    opts, args = parser.parse_args(cmdline)

    
    
    print opts.inputfile
    print opts.outputfile

    # now, call enhancement subprocess

    #proc = subprocess.Popen(['../x64/Debug/Enhancement.exe', '--inputfile', juliusTest, '--outputfile', opts.outputfile], 
    #                        stdout=subprocess.PIPE)

    ## print Enhancement.exe output
    #for line in proc.stdout:
    #    print line

    #juliusProc = subprocess.Popen([juliusExe, '-C', juliusConfig, '-input', 'rawfile'],
    #                              stdin=subprocess.PIPE,
    #                              stdout=subprocess.PIPE,
    #                              stderr=subprocess.STDOUT)

    ##juliusOutput = juliusProc.communicate(juliusTest)
    #juliusOutput = juliusProc.communicate(opts.outputfile)
    #juliusProc.wait()

    ## print julius output
    #for line in juliusOutput:
    #    print line


    #open and parse txt file
    #open and parse outfile

    #compute levenshtein(source, target)
    print levenshtein("That is test".split(), "This is not test".split())

    str = "sentence1: <s> DIAL FIVE FIVE TWO FOUR THREE SIX NINE </s>"

    print str.split(" ")[2:-1]

    return 0


def make_parser():
    """Construct an option parser"""

    usage = """%enhancerPython.py args
            -i, --inputfile     - specify input speech file
            -o, --outputfile    - specify enhanced speech output file
            -e, --enhance       - turn on speech enhancement
Sometimes you might explain the purpose of this program as well.
"""
    
    parser = OptionParser(usage)

    parser.add_option("-i", "--inputfile", help="input wavefile")
    parser.add_option("-o", "--outputfile", help="output wavefile")
    parser.add_option("-e", "--enhance", help="turn on speech enhancement",
                      action="store_true", dest="enhance")

    parser.set_defaults(inputfile="c:\\test.wav", 
                        outputfile="output.wav",
                        enhance=False)

    return parser

    
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))


