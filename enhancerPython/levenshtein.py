def levenshtein(seq1, seq2):
    """
    Perform levenshtein(reference, hypothesis)
    """
    #addition = 0
    #substitution = 0
    #deletion = 0
    oneago = None
    thisrow = range(1, len(seq2) + 1) + [0]
    for x in xrange(len(seq1)):
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in xrange(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
            #if addcost == min(delcost, addcost, subcost):
            #    addition = addition + 1
            #    print "addition"
            #elif delcost == min(delcost, addcost, subcost):
            #    deletion = deletion + 1
            #    print "deletion"
            #elif subcost == min(delcost, addcost, subcost):
            #    substitution = substitution + 1
            #    print "substitution"

    return thisrow[len(seq2) - 1]