# README #

This README specifies some important steps in project development and it defines the TODO list.

speech enhancement project

TODO:

MFCC parametrization

create DEBUG flag and add dumping of the most important variables

   noise spectrum single freq bin	OK - that may be extracted from the spectrogram
   noise estimation time wave		OK
   noisy spectrogram				OK
   noise spectrogram				OK

   and 

   clean and enhanced speech spectrogram OK
   clean and enhanced spectrum OK

create matlab script for spectrum analysis

create evaluation script using objective methods

a priori snr thresholding -15 +15 dB
